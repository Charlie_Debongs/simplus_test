const express = require("express");
const app = express();
const bodyParser = require("body-parser");

app.use(bodyParser.json());

app.get('/', (req, res) => {
    res.send("Weaver library login access...");
})

function checkSimplusEmail(email) {
    var ext = "@simplusinnovation.com"
    if (email.indexOf(ext) > -1) {
        return true;
      } else {
        return false;
      }
}

app.post('/login', (req, res) => {


    console.log(req.body);
    var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm;
    var checker = checkSimplusEmail(req.body.email)

    if (req.body.email == '' || !re.test(req.body.email) || checkSimplusEmail(req.body.email) == false ) {
        console.log('Please enter a valid email address.');
        res.send('False');
    }else {
        console.log("Logged in successfully !!!")
        res.send("True");
    }
    
})

app.listen(3000, () => {
    console.log("Server is up and running...");
})